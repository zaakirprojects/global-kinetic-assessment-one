/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SampleTest;

/**
 *
 * @author Zaakir
 */
public class Pageobjects 
{
    //URL Link
    public static String strPageURL()
    {
        return "https://www.globalkinetic.com";
    }
    
    //MenuXpath 
    public static String MenubuttonXpath()
    {
        return "//i[@class='nav-toggle icon icon-nav-toggle']";
    }
    
    //Contact xpath
    public static String ContactLinkXpath()
    {
        return "//li[@data-menuanchor='contact']/a";
    }
    
    //message title xpath
    public static String MessagetitleXpath()
    {
        return "//input[@id='title']";
    }
    
    //name textbox xpath
    public static String nameXpath()
    {
        return "//input[@id='name']";
    }
    
    //email textbox xpath
    public static String emailXpath()
    {
        return "//input[@id='email']";
    }
    
    //message textbox xpath
    public static String messageXpath()
    {
        return "//textarea[@id='message']";
    }
}
