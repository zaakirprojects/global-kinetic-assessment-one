/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SampleTest;

import java.io.File;
import org.openqa.selenium.By;
/**
 *
 * @author Zaakir
 */
public class GlobalKineticTest {
    
    //Declarem variables
    File fileIEDriver;
    File fileChromeDriver;
    String strbrowserType;

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        System.setProperty("java.awt.headless", "false");
        
        //Handles all driver related functions
        Functions Function = new Functions();
        
        //Retrieves all drivers
        Function.GetDriver();
        
        //Starts parameterised driver (Chrome,IE)
        Function.startDriver("Chrome");
        
        //Navigate to URL (Link in Pageobjects)
        Function.SeleniumDriver.navigate().to(Pageobjects.strPageURL());
        
        //Click Menu button on page (Xpath in Pageobjects)
        Function.SeleniumDriver.findElement(By.xpath(Pageobjects.MenubuttonXpath())).click();
        
        //Static wait to slow down automation flow
        Function.pause(2000);
        
        //Click Contact link (Xpath in PageObjects)
        Function.SeleniumDriver.findElement(By.xpath(Pageobjects.ContactLinkXpath())).click();
        
        //Static wait to slow down automation flow
        Function.pause(2000);
        
        //Enter text in Message Title (Xpath in Page Objects) 
       Function.SeleniumDriver.findElement(By.xpath(Pageobjects.MessagetitleXpath())).sendKeys("Testing Message Title");

       //Static wait to slow down automation flow
       Function.pause(2000);
       
       //Enter text in Name field (Xpath in PageObject)
        Function.SeleniumDriver.findElement(By.xpath(Pageobjects.nameXpath())).sendKeys("Testing name field");
        
        //Static wait to slow down automation flow
        Function.pause(2000);
        
        //Enter text in Email field (Xpath in PageObject)
        Function.SeleniumDriver.findElement(By.xpath(Pageobjects.emailXpath())).sendKeys("Testing email field");
        
        //Static wait to slow down automation flow
        Function.pause(2000);
        
        //Enter text in message field(Xpath in PageObject)
        Function.SeleniumDriver.findElement(By.xpath(Pageobjects.messageXpath())).sendKeys("Testing message field");
        
        //Static wait to slow down automation flow
        Function.pause(2000);
        
        //Close driver connection close browser
        Function.SeleniumDriver.close();
        Function.SeleniumDriver.quit();
    }
}
