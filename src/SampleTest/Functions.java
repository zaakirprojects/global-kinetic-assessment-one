/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SampleTest;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 *
 * @author Zaakir
 */
public class Functions 
{
    File fileIEDriver;
    File fileChromeDriver;
    String strbrowserType;
    public WebDriver SeleniumDriver;
    
    
    public void GetDriver() 
    {
        //Get IE driverserver
        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        //Get Chrome driver server
        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
    }
    
    
    //Start driver server
    public void startDriver(String browserType) 
    {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
        
        switch (browserType) 
        {
            case "IE":
                SeleniumDriver = new InternetExplorerDriver();
                break;
            case "FireFox":
                SeleniumDriver = new FirefoxDriver();
                break;
            case "Chrome":
                SeleniumDriver = new ChromeDriver();
                break;

        }

        SeleniumDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        SeleniumDriver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        SeleniumDriver.manage().timeouts().setScriptTimeout(1, TimeUnit.SECONDS);
        SeleniumDriver.manage().window().maximize();

    }
    
    //pause, sleep, wait 
    public void pause(int milisecondsToWait) 
    {
        try 
        {
            Thread.sleep(milisecondsToWait);
        } 
        catch (Exception e)
        {

        }
    }   
}
